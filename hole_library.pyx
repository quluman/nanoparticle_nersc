#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True, initializedcheck = False


cimport constants as c
from particle_class cimport Sample, Nanoparticle, Electrode, Charge, s_Charge
from event_class cimport Hopping, s_Hopping
cimport utility as util
import numpy as np
cimport numpy as np
from libc.math cimport sqrt, fmin
from libc.math cimport fabs
from libc.math cimport exp
from libc.math cimport round


#######cpdef INT_t throw_hole(pc.nanoparticle[:] nanops, pc.charge[:] holes, FLOAT_t[:] rand):
cdef INT_t throw_hole(Nanoparticle[:] nanops, s_Charge[:] holes, Sample sample):
    cdef INT_t state, i, j, k, tries, holeindex, maxtry
    #######cdef INT_t count = int(rand[0])
    cdef bint success
    #cdef int maxtry
    maxtry = 1000
    # 1. determine where to throw hole randomly, they can land on the
    # drain and source as well when there is pbc
    # 2. fill up states from below
    # 3. if the np is filled we draw a new random number
    for holeindex in range(sample.nhole):    
        success = False
        tries = 0
        while success == False:
            if (tries > maxtry):
                print('try exceeded maxtry!')
                break
            i =  int(np.random.random_integers(0,(nanops.shape[0]-1)))
            #######i = int(rand[count]*nanops.shape[0])
            #######count = count + 1
            
            j = 0
            # It is not very right here, only works for at most two energy levels. First level 0, second level 1 etc.
            if ( nanops[i].vborboccupation[j] == nanops[i].vborbmaxoccupation[j] ):                    
                j=j+1
            if ( j < sample.nbnd ):
                success = True    
            tries = tries + 1

        holes[holeindex].particleindex = i
        holes[holeindex].orbitalindex = j
        nanops[i].vborboccupation[j] = nanops[i].vborboccupation[j] + 1
        nanops[i].vboccupation = nanops[i].vboccupation + 1
        
        # Now testing memoview, it has no .sum() feature, but it is ok now since there is only 1 band
        #state = nanops[i].orbmaxoccupation.sum() - nanops[i].orbmaxoccupation[j] + nanops[i].orboccupation[j]
        state = 0        
        for k in range(sample.nbnd):
            state = state + nanops[i].vborbmaxoccupation[k]
            
        state = state - nanops[i].vborbmaxoccupation[j] + nanops[i].vborboccupation[j] - 1       
        nanops[i].holeindex[state] = holeindex
        holes[holeindex].state = state
    
    
    #######rand[0] = float(count)    
    
    return 0
    


cdef INT_t find_hole_events_size(Nanoparticle[:] nanops, Charge[:] holes, Sample sample):

    
    cdef INT_t i, j, l, sp, so, tp, to
    
    
    # Get the size of hopping events
    l = 0
    for i in range(holes.shape[0]):
        sp = holes[i].particleindex
        so = holes[i].orbitalindex 
        #loop over neighbor particles
        for j in range(nanops[sp].nn):
            tp = nanops[sp].nnindex[j]
            # and look for free slots 
            for to in range(sample.nbnd):
                if (nanops[tp].vborboccupation[to] < nanops[tp].vborbmaxoccupation[to]):                    
                    l += 1
    
    return l
    
    
    
    
cdef INT_t find_hole_events(Nanoparticle[:] nanops, Charge[:] holes, Hopping[:] holehoppings, Sample sample):

    cdef INT_t i, j, l, sp, so, tp, to
    cdef FLOAT_t hrate
    #cdef ec.hopping_event new_event
    cdef FLOAT_t holecharging0energy  
    cdef FLOAT_t holechargingenergy
    
    
    
    # Setting up the hole events. Note set l=-1 here to ensure counting from 0 in the body
    l = -1
    # loop over electrons  
    for i in range(holes.shape[0]):
        sp = holes[i].particleindex
        so = holes[i].orbitalindex 
        #loop over neighbor particles
        for j in range(nanops[sp].nn):
            tp = nanops[sp].nnindex[j]
            # and look for free slots 
            for to in range(sample.nbnd):
                if (nanops[tp].vborboccupation[to] < nanops[tp].vborbmaxoccupation[to]):                    
                    l += 1
                    ###holehoppings[l] = ec.hopping_event(1)   # 1 for hole hopping
                    holehoppings[l].sourceparticle = sp
                    holehoppings[l].sourceorbital = so
                    holehoppings[l].targetparticle = tp
                    holehoppings[l].targetorbital = to
                    holehoppings[l].holeindex = i
                    # add kinetic energy difference
                    holehoppings[l].energydiff = nanops[tp].vbenergy[to]-nanops[sp].vbenergy[so]
                    # add on-site (self) energy difference
                    if (sample.lcapacitance0):                        
                        # this function adds up the contribution
                        # due to the load of the first charge
                        holecharging0energy = nanops[tp].hselfenergy0 - nanops[sp].hselfenergy0
                        holehoppings[l].energydiff = holehoppings[l].energydiff + holecharging0energy

                    # this function adds up contribution from the self-energy due to the 
                    # load of an additional (at least the second) charge
                    
                    holechargingenergy = nanops[tp].vboccupation*nanops[tp].hselfenergy-(nanops[sp].vboccupation-1)*nanops[sp].hselfenergy

                    holehoppings[l].energydiff = holehoppings[l].energydiff + holechargingenergy
                    # add electron-hole interaction
                    holehoppings[l].electronhole = electronholeeh(nanops, holehoppings[l])
                    holehoppings[l].energydiff = holehoppings[l].energydiff + holehoppings[l].electronhole
  ###                  # add external potential 
                    holehoppings[l].energydiff = holehoppings[l].energydiff + holes[i].charge*sample.voltage/sample.cellz*(nanops[tp].z-nanops[sp].z-sample.cellz*round((nanops[tp].z-nanops[sp].z)/sample.cellz))

 ###                   # add built-in field
                    #holehoppings[l].energydiff = holehoppings[l].energydiff + holes[i].charge/sqrt(2)*(sourcewf-drainwf)/cellz*(nanops[tp].z-nanops[sp].z)
                    # add electrotatic energy difference due to other dots        

########################
##!! No Possion yet !!##
########################
##  holehoppings[l].poisson=hpoisson(holehoppings[l])
##  holehoppings[l].energydiff = holehoppings[l].energydiff + holehoppings[l].poisson
                    
                    # calculate rate and multiply it with the final state degeneracy
                    # initial state degeneracy is taken into account by summing over all electrons
                    # room for optimization: only sum over particles and explicitly take into
                    # account initial state degeneracy, this becomes important if there is more
 ###                   # than one electron per nanoparticle
                    holehoppings[l].rate=(nanops[tp].vborbmaxoccupation[to]-nanops[tp].vborboccupation[to])*get_hrate(nanops, holehoppings[l], sample)
    #print(hoppings)   
    #cf.hole_hoppings = np.asarray(holehoppings)
    # (l+1) is number of electron hopping events
    return l+1    
    

cdef INT_t execute_hole_event(Nanoparticle[:] nanops, s_Charge[:] holes, s_Hopping executedhopping):
  
    cdef INT_t i, j, state
    cdef INT_t tp, to, sp, so
    cdef INT_t current
    
    tp = executedhopping.targetparticle
    to = executedhopping.targetorbital
    sp = executedhopping.sourceparticle
    so = executedhopping.sourceorbital
    
    nanops[tp].hotness = nanops[tp].hotness + 1 
   
    current = 0
        
    state = holes[executedhopping.holeindex].state

    nanops[sp].holeindex[state] = -1

    #state = findelecnewstate(executedhopping.targetparticle,executedhopping.targetorbital)
    #state = nanops[tp].orbmaxoccupation.sum() - nanops[tp].orbmaxoccupation[to] + nanops[tp].orboccupation[to]    
    state = findholenewstate(nanops, tp, to)
        
    nanops[tp].holeindex[state] = executedhopping.holeindex

    nanops[sp].vborboccupation[so] = nanops[sp].vborboccupation[so] - 1
    
    nanops[tp].vborboccupation[to] = nanops[tp].vborboccupation[to] + 1
    
    nanops[sp].vboccupation = nanops[sp].vboccupation - 1
    
    nanops[tp].vboccupation = nanops[tp].vboccupation + 1
   
    # update hopping hole

    holes[executedhopping.holeindex].particleindex = tp
    holes[executedhopping.holeindex].orbitalindex = to
    holes[executedhopping.holeindex].state = state

    if (nanops[tp].drain and nanops[sp].source):

        current = -1

    if (nanops[sp].drain and nanops[tp].source):

        current = 1

    return current



### Funtions used in this part
 
   
cdef FLOAT_t electronholeeh(Nanoparticle[:] nanops, s_Hopping hopping):

    cdef INT_t sp, tp
    cdef FLOAT_t temp, exconsp, excontp

    sp = hopping.sourceparticle
    tp = hopping.targetparticle
    exconsp = fmin(nanops[sp].vboccupation,nanops[sp].occupation)
    excontp = fmin(nanops[tp].vboccupation,nanops[tp].occupation+1)
    # first let's take care of the self energy cancellation
    # due to the first electron-hole pair
    temp = 0
    if (excontp > 0):
        temp = -nanops[tp].selfenergy0
    if (exconsp > 0): 
      # minus*minus=plus
        temp = temp + nanops[sp].selfenergy0
    # then take care of the self energy cancellation of additional electron-hole pairs, the multiple
    # exciton binding energy is just the sum of single exciton binding energies
    exconsp = fmin(nanops[sp].vboccupation,nanops[sp].occupation-1)
    excontp = fmin(nanops[tp].vboccupation,nanops[tp].occupation)

    temp = temp - excontp*nanops[tp].selfenergy + exconsp*nanops[sp].selfenergy

    return temp
  

cdef FLOAT_t get_hrate(Nanoparticle[:] nanops, s_Hopping hopping, Sample sample):
    cdef FLOAT_t overlap, rrate, rate0, energytmp
    cdef INT_t tp, to, sp, so
    
    tp = hopping.targetparticle
    to = hopping.targetorbital
    sp = hopping.sourceparticle
    so = hopping.sourceorbital    
    
    rate0 = 0
    rrate = 0
    # in Ry atomic units m=1/2, e^2=2, hbar=1
    # used here Chandler's approximation with the average energies
    # is this mass the electron/hole effective mass? than it should be different for holes and electrons
    overlap = sqrt(-sample.hmass*(nanops[sp].vbenergy[so] + nanops[tp].vbenergy[to]) / 2.0)

    if (sample.lmarcus):
        rate0 = c.tpi*sample.marcusprefac2*exp(-2.0 * util.npnpdistance(nanops[sp], nanops[tp],sample) * overlap) / sqrt(c.fpi*sample.reorgenergy*sample.temperature)
        rrate = rate0*exp(-(sample.reorgenergy+hopping.energydiff)**2.0/(4.0*sample.reorgenergy*sample.temperature))

    elif (sample.lma):
        rate0 = sample.jumpfreq*exp(-2.0 * util.npnpdistance(nanops[sp], nanops[tp],sample)*overlap)

        if hopping.energydiff > 0.0 :
            # linearized in external field
            #
            # [Marton: for holes the sign should be positive
            #          energytmp=holes[i].charge*sample.voltage/cellz*(nanops[tp].z-nanops[sp].z)
            #          or energytmp=sqrt(2)*voltage/cellz*(nanops[tp].z-nanops[sp].z)]
            #
            #energytmp = -sqrt(2)*voltage/cellz*(nanops[tp].z-nanops[sp].z)
            #hopping.energydiff = hopping.energydiff - energytmp
            #rrate = rate0*exp(-hopping.energydiff/temperature)*fabs(1.0 -energytmp/temperature)
            #
            # original Miller-Abrahams formula
            rrate=rate0*exp(-hopping.energydiff/sample.temperature)
        else: 
            rrate = rate0
    return rrate


    
    
    
cdef INT_t findholenewstate(Nanoparticle[:] nanop, INT_t p, INT_t o):
    # p is nanoparticle number, o is orbital number
    cdef INT_t states
    cdef INT_t i
    
    states = 0
    
    for i in range(nanop[p].vborbmaxoccupation.shape[0]):
        states = states + nanop[p].vborbmaxoccupation[i]
    
    states = states -nanop[p].vborbmaxoccupation[o] + nanop[p].vborboccupation[o]    
    return states



cdef INT_t find_hole_events_cluster(Nanoparticle[:] nanops, s_Charge[:] holes, s_Hopping[:] holehoppings, Sample sample): #, pc.Cluster[:] clusterz):

    cdef:
        INT_t i, j, l, sp, so, tp, to, cluster_num
        FLOAT_t hrate
        FLOAT_t holecharging0energy  
        FLOAT_t holechargingenergy
        FLOAT_t cellz
        
    cellz = sample.cellz
        
    # Setting up the hole events. Note set l=-1 here to ensure counting from 0 in the body
    l = -1
    # loop over holes 
    for i in range(sample.nhole):
        sp = holes[i].particleindex
        so = holes[i].orbitalindex 
        
        ###########################
        # Simplified case version #
        ###########################
        
        for j in range(nanops[sp].nn):
            tp = nanops[sp].nnindex[j]
            # and look for free slots 
            for to in range(sample.nbnd):
                if (nanops[tp].vborboccupation[to] < nanops[tp].vborbmaxoccupation[to]):                    
                    l += 1                                       
                    holehoppings[l].sourceparticle = sp
                    holehoppings[l].sourceorbital = so
                    holehoppings[l].targetparticle = tp
                    holehoppings[l].targetorbital = to
                    holehoppings[l].holeindex = i
                    
                    if nanops[sp].if_hcn[j] == 0:
                        # add kinetic energy difference
                        holehoppings[l].energydiff = nanops[tp].vbenergy[to]-nanops[sp].vbenergy[so]
                        # add on-site (self) energy difference
                        if (sample.lcapacitance0):                        
                            # this function adds up the contribution
                            # due to the load of the first charge
                            holecharging0energy = nanops[tp].hselfenergy0 - nanops[sp].hselfenergy0
                            holehoppings[l].energydiff = holehoppings[l].energydiff + holecharging0energy

                        # this function adds up contribution from the self-energy due to the 
                        # load of an additional (at least the second) charge
                        
                        holechargingenergy = nanops[tp].vboccupation*nanops[tp].hselfenergy-(nanops[sp].vboccupation-1)*nanops[sp].hselfenergy

                        holehoppings[l].energydiff = holehoppings[l].energydiff + holechargingenergy
                        # add electron-hole interaction
                        holehoppings[l].electronhole = electronholeeh(nanops, holehoppings[l])
                        holehoppings[l].energydiff = holehoppings[l].energydiff + holehoppings[l].electronhole
                        # add external potential 
                        holehoppings[l].energydiff = holehoppings[l].energydiff + holes[i].charge*sample.voltage/cellz*(nanops[tp].z-nanops[sp].z-cellz*round((nanops[tp].z-nanops[sp].z)/cellz))

                        # add built-in field
                        #holehoppings[l].energydiff = holehoppings[l].energydiff + holes[i].charge/sqrt(2)*(sourcewf-drainwf)/cellz*(nanops[tp].z-nanops[sp].z)
                        # add electrotatic energy difference due to other dots        


                        holehoppings[l].rate=(nanops[tp].vborbmaxoccupation[to]-nanops[tp].vborboccupation[to])*get_hrate(nanops, holehoppings[l], sample)
           

                    if nanops[sp].if_cn[j] == 1: # This is the intra-cluster hopping version
                        
                        # Allow hoppings towards source only
                        if -(nanops[tp].z-nanops[sp].z-cellz*round((nanops[tp].z-nanops[sp].z)/cellz)) > 0 :                       
                            
                            l += 1
                            holehoppings[l].sourceparticle = sp
                            holehoppings[l].sourceorbital = so
                            holehoppings[l].targetparticle = tp
                            holehoppings[l].targetorbital = to
                            holehoppings[l].electronindex = i
                            
                            holehoppings[l].rate = (nanops[tp].vborbmaxoccupation[to]-nanops[tp].vborboccupation[to])* (sample.fitting) * sample.voltage /cellz*(nanops[tp].z-nanops[sp].z-cellz*round((nanops[tp].z-nanops[sp].z)/cellz))
                            
                       

                        # Here we need to think it really carefully. The rate should be inversely proportional to the travel distance, and multiply with a fitting parameter
                        # Refer to Matt Law figure?
    
        

    return l+1    






