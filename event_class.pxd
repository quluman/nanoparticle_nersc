cimport numpy as np

ctypedef np.float64_t FLOAT_t
ctypedef np.int_t INT_t
ctypedef np.long_t INDEX_t
ctypedef np.int8_t BOOL_t


cdef class Hopping:
    cdef INT_t electronindex
    cdef INT_t holeindex  
    cdef INT_t chargeindex     
    cdef INT_t sourceparticle          
    cdef INT_t sourceorbital          
    cdef INT_t targetparticle
    cdef INT_t targetorbital  
    cdef FLOAT_t energydiff
    cdef FLOAT_t rate
    cdef FLOAT_t poisson
    cdef FLOAT_t electronhole
    cdef INT_t hopping_type
    cdef FLOAT_t totalrate
    cdef INT_t intra
    
    
cdef struct s_Hopping:
    int electronindex
    int holeindex  
    int chargeindex     
    int sourceparticle          
    int sourceorbital          
    int targetparticle
    int targetorbital  
    double energydiff
    double rate
    double poisson
    double electronhole
    int hopping_type
    double totalrate
    int intra
    
    
     