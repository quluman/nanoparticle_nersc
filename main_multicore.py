



#!/home/quluman/anaconda3/bin/python3

import multiprocessing as mp
import matplotlib.pyplot as plt
import numpy as np
import math
#import utility as util   
import sys
from monte_carlo import monte_carlo


######################################################################
## Multi-threaded functions, need to call the single-thread version ##
######################################################################






# Get the mobility as a function of temperature, range [50, 400] 
def simulation_mobility(number_of_e, number_of_np, energy_threshold, sample_start):
 # Define an output queue
    feature = 'mobility'     # (mobility or iv)
    steps = 500000
    temp_steps = 8        # Fixed at 8 here for now
    sample_numbers = 100   #  
    #voltage = int((0.015*1000)+0.5)/1000       # in Volts
    no_electrons = int(number_of_e)
    no_holes = 0
    no_nanoparticles = int(number_of_np)
  
      
    #temp_list = [30, 60, 90, 120, 160, 200, 240, 300]

    #temp_list = [40, 50, 70, 80]

    temp_list = [30, 31, 32, 33, 34, 35, 37, 39]
 
    simultaneous_samples = 4 # one sample requires 8 cores
    
    rounds = sample_numbers / simultaneous_samples

    # result_all is a list of mobility. each row is for a single sample, each column is for a different temperature point
    results_all = np.zeros([sample_numbers,len(temp_list)])
    
    # Setup a list of processes that we want to run    
    for round_number in range(int(rounds)): 
        
        # Just to keep track
        print('no. of electrons, holes, nanoparticles = %3d, %3d, %3d' % (no_electrons,no_holes,no_nanoparticles))
        
        # Contain a list of sample numbers in each round
        samples = []
        
        processes = []
        
        output = mp.Queue()
        
        for s in range(simultaneous_samples):
            
            samples.append(round_number*simultaneous_samples + s)
            
            print('working on sample # %2d/%2d with threshold %3f' %(samples[s]+1, sample_numbers, energy_threshold))
            
            # args = (INT_t steps, INT_t sample, INT_t e_number, INT_t h_number, FLOAT_t temp, FLOAT_t thr, str features, output)
            # This is one sample with eight T steps
            processes += [mp.Process(target = monte_carlo, args=(steps, samples[s], no_electrons, no_holes, no_nanoparticles, temp_list[x], energy_threshold, feature, 0, output) ) for x in range(len(temp_list))] 
                        
        #output.put([sample, temp, ave_mobility/5000])

        # Run processes
        for p in processes:
            p.start()

        # Exit the completed processes
        for p in processes:
            p.join()

        # Get process results from the output queue
        results = np.asarray([output.get() for p in processes])                       
        #print(results)
                
        
        # Get the results ready for output        
        for s in range(len(samples)):
            
            # Get the sample number index
            sample_index = samples[s] 
            
            # get a temporary list of results for a particular sample
            results_sample = results[results[:,0] == sample_index]
            
            # arrange the mobility results of this particular sample in ascending temperature
            for i in range(len(temp_list)):
                for j in range(len(temp_list)):
                    if results_sample[j][1] == temp_list[i]:              
                        results_all[sample_index][i] = results_sample[j][2]
            

    filename = 'e_'+str(no_electrons)+'_h_'+str(no_holes)+'_thr_'+str(energy_threshold)+'_samp_'+str(sample_numbers)
    
    np.savetxt('output/'+filename, results_all)
    
    #util.mobility_plot(filename, temp_list, results_all)
        
    return results_all



def simulation_iv(number_of_e, number_of_np, energy_threshold, sample_start, T):
 # Define an output queue
    feature = 'iv'     # (mobility or iv)
    steps = 500000
    sample_numbers = 100   #  
    #voltage = int((0.015*1000)+0.5)/1000       # in Volts
    no_electrons = int(number_of_e)
    no_holes = 0
    no_nanoparticles = int(number_of_np)
    
    temperature = T
  

    voltage_ratio = [0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07]
    
    voltage_steps = len(voltage_ratio)
 
    simultaneous_samples = 4 # one sample requires 8 cores
    
    rounds = sample_numbers / simultaneous_samples

    # result_all is a list of mobility. each row is for a single sample, each column is for a different temperature point
    results_all = np.zeros([sample_numbers,voltage_steps])
    
    # Setup a list of processes that we want to run    
    for round_number in range(int(rounds)): 
        
        # Just to keep track
        print('no. of electrons, holes, nanoparticles = %3d, %3d, %3d' % (no_electrons,no_holes,no_nanoparticles))
        
        # Contain a list of sample numbers in each round
        samples = []
        
        processes = []
        
        output = mp.Queue()
        
        for s in range(simultaneous_samples):
            
            samples.append(round_number*simultaneous_samples + s)
            
            print('working on sample # %2d/%2d with threshold %3f' %(samples[s]+1, sample_numbers, energy_threshold))
            
            # args = (INT_t steps, INT_t sample, INT_t e_number, INT_t h_number, FLOAT_t temp, FLOAT_t thr, str features, output)
            # This is one sample with eight T steps
            processes += [mp.Process(target = monte_carlo, args=(steps, samples[s], no_electrons, no_holes, no_nanoparticles, temperature, energy_threshold, feature, voltage_ratio[x], output) ) for x in range(len(voltage_ratio))] 
                        
        #output.put([sample, temp, ave_mobility/5000])

        # Run processes
        for p in processes:
            p.start()

        # Exit the completed processes
        for p in processes:
            p.join()

        # Get process results from the output queue
        results = np.asarray([output.get() for p in processes])                       
        #print(results)
                
        
        # Get the results ready for output        
        for s in range(len(samples)):
            
            # Get the sample number index
            sample_index = samples[s] 
            
            # get a temporary list of results for a particular sample
            results_sample = results[results[:,0] == sample_index]
            
            # arrange the mobility results of this particular sample in ascending temperature
            for i in range(len(voltage_ratio)):
                for j in range(len(voltage_ratio)):
                    if results_sample[j][1] == voltage_ratio[i]:              
                        results_all[sample_index][i] = results_sample[j][2]
            

    filename = 'e_'+str(no_electrons)+'_h_'+str(no_holes)+'_thr_'+str(energy_threshold)+'_samp_'+str(sample_numbers)+'_T_'+str(temperature)
    
    np.savetxt('output/'+filename, results_all)
    
    #util.mobility_plot(filename, temp_list, results_all)
        
    return results_all





#############################################
## Below are the single-threaded functions ##
#############################################




########
# main #
########

def mobility_series():
    
    electron_number = [50] 
    
    for e_no in electron_number:
        
        thr = np.arange(5)*0.01 + 0.3 
        
        for i in range(thr.shape[0]):        
            
            result = simulation_mobility(e_no, 400,thr[i], 0) # args = (number_of_e, energy_threshold, first_sample)
    
    #simulation_mobility(number_of_e, energy_threshold, sample_start):


def mobility_series_input():
    
    electron_number = int(float(sys.argv[1]))
    
    np_number = int(float(sys.argv[2]))
    
    thr_start = float(sys.argv[3])
    
    thr_steps = float(sys.argv[4])
    
    thr_resolution = float(sys.argv[5])

    thr = np.arange(int(thr_steps))*thr_resolution + thr_start
    
    for i in range(thr.shape[0]):
        
        result = simulation_mobility(electron_number, np_number, thr[i], 0) # args = (number_of_e, energy_threshold, first_sample)
    

def iv_input():
    
    electron_number = int(float(sys.argv[1]))
    
    np_number = int(float(sys.argv[2]))
    
    thr_start = float(sys.argv[3])
    
    thr_steps = float(sys.argv[4])
    
    thr_resolution = float(sys.argv[5])
    
    T = float(sys.argv[6])

    thr = np.arange(int(thr_steps))*thr_resolution + thr_start
    
    for i in range(thr.shape[0]):
  
        result = simulation_iv(electron_number, np_number, thr[i], 0, T) # args = (number_of_e, number_of_np, energy_threshold, sample_start, T)
    




#mobility_series()

#mobility_series_input()

iv_input()


