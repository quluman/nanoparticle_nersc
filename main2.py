



#!/home/quluman/anaconda3/bin/python3

import multiprocessing as mp
import matplotlib.pyplot as plt
import numpy as np
import math
import utility as util   
import sys
from monte_carlo import monte_carlo, read_packing, find_percolation


######################################################################
## Multi-threaded functions, need to call the single-thread version ##
######################################################################


# simplified phase simulation, just get the mobility at 400K and 50K
def simulation_phase(number_of_e, energy_threshold, sample_start):
 # Define an output queue
    feature = 'mobility'     # (mobility or iv)
    steps = 400000
    temp_steps = 2        # Fixed at 2 here for now
    sample_numbers = 10   # Number of pairs here
    #voltage = int((0.015*1000)+0.5)/1000       # in Volts
    no_electrons = int(number_of_e)
    no_holes = 0
    no_nanoparticles = 400
    
    
    temp_list = [50, 400] # Just need these two temperature points
    
    #for x in range(temp_steps):
    #    temp_list.append(x*50+50)

    simultaneous_samples = 2

    # result_pair is a list of mobility. each row is for a pair of samples. first col is mobility at 50K, second col is mobility at 400K
    results_pair = np.zeros([sample_numbers,temp_steps])
    
    # Setup a list of processes that we want to run    
    
    for sample in range(sample_numbers): # each sample consists of two arrangements, regular and reverse
        
        # Generate random numbers for simulation
        
        #util.generate_random(sample, len(temp_list)*simultaneous_samples, steps, energy_threshold)   #(INT_t sample_number, INT_t file_number, INT_t step_number):
        
        print('working on sample # %2d/%2d with threshold %3f' %(sample+1, sample_numbers, energy_threshold))
        print('no. of electrons, holes, nanoparticles = %3d, %3d, %3d' % (no_electrons,no_holes,no_nanoparticles))  
        
        output = mp.Queue()

        #main(INT_t steps, INT_t sample, INT_t e_number, INT_t h_number, FLOAT_t temp, FLOAT_t thr, str features, output)
        #output.put([sample, temp, ave_mobility/5000])

        # This is the normal sample with two T steps
        processes = [mp.Process(target = monte_carlo, args=(steps, (sample+sample_start)*2, no_electrons, no_holes, no_nanoparticles, temp_list[x], energy_threshold, feature, output) ) for x in range(len(temp_list))]  # args = (voltage, x, sample, output, steps, e_thr, Temp, feat)
        
        # This is the reverse sample with two T steps
        processes += [mp.Process(target = monte_carlo, args=(steps, ((sample+sample_start)*2+1), no_electrons, no_holes, no_nanoparticles, temp_list[x], energy_threshold, feature, output) ) for x in range(len(temp_list))]

        # Run processes
        for p in processes:
            p.start()

        # Exit the completed processes
        for p in processes:
            p.join()

        # Get process results from the output queue
        results = np.asarray([output.get() for p in processes])                       
        #print(results)
                
        # Calculate the output pair values
        # result_pair is a list of mobility. each row is for a pair of samples. first col is mobility at 50K, second col is mobility at 400K
        for i in range(len(temp_list)):
            results_pair[sample,i] = results[results[:,1] == temp_list[i]][:,2].sum()/2
            
        #print(results_pair)
 

    return results_pair



# Get the mobility as a function of temperature, range [50, 400] 
def simulation_mobility(number_of_e, number_of_np, energy_threshold, sample_start):
 # Define an output queue
    feature = 'mobility'     # (mobility or iv)
    steps = 500000
    temp_steps = 8        # Fixed at 8 here for now
    sample_numbers = 20   # 
    #voltage = int((0.015*1000)+0.5)/1000       # in Volts
    no_electrons = int(number_of_e)
    no_holes = 0
    no_nanoparticles = int(number_of_np)
  
    temp_list = []
    
    for x in range(temp_steps):
        temp_list.append(x*50+50)

    simultaneous_samples = 2

    # result_all is a list of mobility. each row is for a single sample, each column is for a different temperature point
    results_all = np.zeros([sample_numbers,temp_steps])
    
    # Setup a list of processes that we want to run    
    
    for sample in range(sample_numbers): 
        
        
        print('working on sample # %2d/%2d with threshold %3f' %(sample+1, sample_numbers, energy_threshold))
        print('no. of electrons, holes, nanoparticles = %3d, %3d, %3d' % (no_electrons,no_holes,no_nanoparticles))  
        
        output = mp.Queue()

        #main(INT_t steps, INT_t sample, INT_t e_number, INT_t h_number, FLOAT_t temp, FLOAT_t thr, str features, output)
        #output.put([sample, temp, ave_mobility/5000])

        # This is the normal sample with two T steps
        processes = [mp.Process(target = monte_carlo, args=(steps, (sample+sample_start), no_electrons, no_holes, no_nanoparticles, temp_list[x], energy_threshold, feature, output) ) for x in range(len(temp_list))] 
        
        # Run processes
        for p in processes:
            p.start()

        # Exit the completed processes
        for p in processes:
            p.join()

        # Get process results from the output queue
        results = np.asarray([output.get() for p in processes])                       
        #print(results)
                
         # Order the results
        for i in range(len(temp_list)):
            for j in range(len(temp_list)):
                if results[j][1] == temp_list[i]:              
                    results_all[sample][i] = results[j][2]
            

    filename = 'e_'+str(no_electrons)+'_h_'+str(no_holes)+'_thr_'+str(energy_threshold)+'_samp_'+str(sample_numbers)
    
    np.savetxt('output/'+filename, results_all)
    
    #util.mobility_plot(filename, temp_list, results_all)
        
    return results_all



# Get the mobility as a function of temperature, range [50, 400] 
def packing():
 # Define an output queue
    sample_numbers = 48  # 
  
    output = []

    # result_all is a list of mobility. each row is for a single sample, each column is for a different temperature point
    results_all = np.zeros(sample_numbers)
    
    # Setup a list of processes that we want to run    
    
    for sample in range(sample_numbers): 
        
       read_packing(sample, 400, output) 
       
    print(output)
    
    result = np.asarray(output)
    
    print(result.mean())
    print(result.std()/np.sqrt(result.shape[0]))
    
       
    return 0




def metallic_portion():
        
    sample_numbers = 50
    
    thr = np.arange(80)*0.01 + 0
    
    thr2 = np.arange(30)*0.005 + 0.15
    
    portion = np.zeros([thr.shape[0], 2])
    
    percolation = np.zeros([thr2.shape[0], 2])
    
    
    for i in range(thr.shape[0]): 
        
        output = []
    
        for sample in range(sample_numbers):

            find_percolation(sample*2, 400, thr[i], output, False)
            
        result = np.asarray(output)
        #print(result)
        #print(result[:,0].mean(axis=0))
        #print(result[:,0].std(axis=0)/ np.sqrt(result.shape[0]))
    
        portion[i,0] = result[:,0].mean(axis=0)
        portion[i,1] = result[:,0].std(axis=0)/ np.sqrt(result.shape[0])
        
        #percolation[i,0] = result[:,1].mean(axis=0)
        #percolation[i,1] = result[:,1].std(axis=0)/ np.sqrt(result.shape[0])
    
        #print(output)
        
    for i in range(thr2.shape[0]): 
        
        output = []
    
        for sample in range(sample_numbers):

            find_percolation(sample*2, 400, thr2[i], output, False)
            
        result = np.asarray(output)
    
        #portion[i,0] = result[:,0].mean(axis=0)
        #portion[i,1] = result[:,0].std(axis=0)/ np.sqrt(result.shape[0])
        
        percolation[i,0] = result[:,1].mean(axis=0)
        percolation[i,1] = result[:,1].std(axis=0)/ np.sqrt(result.shape[0])
    
    
    #print(portion)
    plt.errorbar(thr, portion[:,0], yerr=portion[:,1], label ='metallic bonds portion')
    plt.errorbar(thr2, percolation[:,0], yerr=percolation[:,1], label='percolation probability')
    
    
    
    
    
    
    
    
    
    #plt.ylim([0,1.1])
    plt.xlabel('Portion of FWHM',fontsize = 15)
    plt.ylabel('Portion of metallic bonds / Percolation probability',fontsize = 15)
    plt.title('Metallic bonds ratio / Percolation probability vs allowed threshold',fontsize = 15)
    
    plt.legend(bbox_to_anchor=(1, 0), loc=4, borderaxespad=0.,prop={'size':10})
    
    plt.savefig('percolation_probability')
    plt.show()






#############################################
## Below are the single-threaded functions ##
#############################################




########
# main #
########

def simplified_phase():
     
    #electron_number = float(sys.argv[1])      
    electron_number = [25, 50]    
    
    for e_no in electron_number:
        
        thr = np.arange(5)*0.01+0.1
        output = np.ndarray([thr.shape[0],3])
        
        for i in range(thr.shape[0]):        
            #print(thr[i])            
            # result is a list of mobility. each row is for a pair of samples. first col is mobility at 50K, second col is mobility at 400K
            result = simulation_phase(e_no, thr[i], 0) # args = (number_of_e, energy_threshold, first_sample)
            
            #ratio = np.ndarray(result.shape[0])
            
            mobility_50 = result[:,0].mean()
            
            error_50 = result[:,0].std() / (math.sqrt(result[:,0].shape[0]) * mobility_50 )
            
            mobility_400 = result[:,1].mean()
            
            error_400 = result[:,1].std() / (math.sqrt(result[:,1].shape[0]) * mobility_400 )
            
            #ratio = result[:,1]/result[:,0]    # this gives an array of mobility at 400 / 50 K
            
            output[i,0] = thr[i]                                   # First column is threshold value
            output[i,1] = mobility_400 / mobility_50               # Second column is the mean ratio value 
            output[i,2] = output[i,1] * math.sqrt(error_400**2 + error_50**2)    # Third column is the error in the mean

        
        
        filename = 'e_'+str(e_no)+'_samp_'+str(10)
        
        np.savetxt('output/'+filename, output)


    

def mobility_series():
    
    electron_number = [250] 
    
    for e_no in electron_number:
        
        thr = np.arange(18)*0.02 + 0.16
        
        for i in range(thr.shape[0]):        
            
            result = simulation_mobility(e_no, 200, thr[i], 0) # args = (number_of_e, energy_threshold, first_sample)
    
    #simulation_mobility(number_of_e, energy_threshold, sample_start):
    


#simplified_phase()

mobility_series()

#packing()

#metallic_portion()



