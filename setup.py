from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
import numpy
#import matplotlib.pyplot as plt

exts = [
	Extension("event_class", ["event_class.pyx"], extra_compile_args=['-Wno-cpp', '-Wno-unused-function','-ffast-math']),
        Extension("particle_class", ["particle_class.pyx"],extra_compile_args=['-Wno-cpp', '-Wno-unused-function','-ffast-math']),
        #Extension("cluster_class", ["cluster_class.pyx"]),
        Extension("constants", ["constants.pyx"],extra_compile_args=['-Wno-cpp', '-Wno-unused-function','-ffast-math']),
       	Extension("electron_library", ["electron_library.pyx"],extra_compile_args=['-Wno-cpp', '-Wno-unused-function','-ffast-math']),
	Extension("hole_library", ["hole_library.pyx"],extra_compile_args=['-Wno-cpp', '-Wno-unused-function','-ffast-math']),
	#Extension("sys_init", ["sys_init.pyx"]),
	#Extension("monte_carlo", ["monte_carlo.pyx"]),
	Extension("utility", ["utility.pyx"],extra_compile_args=['-Wno-cpp', '-Wno-unused-function','-ffast-math']),
        Extension("routines", ["routines.pyx"],extra_compile_args=['-Wno-cpp', '-Wno-unused-function','-ffast-math']),
        #Extension("cluster_sim", ["cluster_simulation.pyx"]),
        #Extension("test", ["test.pyx"],extra_compile_args=['-Wno-cpp', '-Wno-unused-function','-ffast-math']),
        Extension("monte_carlo", ["monte_carlo.pyx"],extra_compile_args=['-Wno-cpp', '-Wno-unused-function','-ffast-math']),
        ]

setup(
    cmdclass = {'build_ext': build_ext},
    ext_modules = exts,
    include_dirs=[numpy.get_include()]
)

##
