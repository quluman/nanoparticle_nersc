#!/bin/bash -l

#SBATCH -p regular
#SBATCH -N 1
#SBATCH -t 1:50:00
#SBATCH -J 100_e_0.28

#module load siesta/3.2-pl-5

#srun -n 1 -c 32 ./mycode.exe

# to export shell variables either use -V in SBATCH or...

export PATH=$PATH:/global/homes/q/quluman/anaconda3/bin/

echo $PATH


python3 /global/homes/q/quluman/nanoparticle_nersc_no_nn/main_multicore.py 100 400 0.28 4 0.02


#  electron_number = int(float(sys.argv[1]))    
#  np_number = int(float(sys.argv[2]))    
#  thr_start = float(sys.argv[3])    
#  thr_steps = float(sys.argv[4])
#  thr_resolution = float(sys.argv[5])
