



cdef np.ndarray set_nanoparticles():
    
    
    cdef:
        str filename ='input/nanoparticles/'+ str(sample.nnanops)+'/' + 'nanoparticles'+str(sample.sample_number)+'.inp'    
        list temp
    
    #print('opening sample number ', x)
    
    #f = open('input/nanoparticle.inp', 'r')
    f = open(filename, 'r')
    
    #read degeneracy information
    
    f.readline()
    temp = ((f.readline()).strip()).split()
    #print(temp.split())
    sample.e_degeneracy = int(temp[0])
    sample.h_degeneracy = int(temp[1])
    f.readline()
    
    temp = ((f.readline()).strip()).split(",")
    #print(temp[1])
    sample.cellx = float(temp[1]) * c.nmtobohr
    
    temp = ((f.readline()).strip()).split(",")
    #print(temp[1])
    sample.celly = float(temp[1]) * c.nmtobohr
    
    temp = ((f.readline()).strip()).split(",")
    #print(temp[1])
    sample.cellz = float(temp[1]) * c.nmtobohr
    sample.cellz_nm = float(temp[1])
    

    cdef np.ndarray nanoparticles = np.empty(sample.nnanops, dtype = Nanoparticle)
    
    #cdef pc.nanoparticle[:] nanoparticles 
    
    cdef INT_t i=0
    cdef INT_t j=0
    
    cdef INT_t nbnd = sample.nbnd
    cdef INT_t degeneracy = sample.e_degeneracy
    
   
    cdef:
        np.ndarray[INT_t] orboccupation
        np.ndarray[INT_t] orbmaxoccupation
        np.ndarray[INT_t] vborboccupation
        np.ndarray[INT_t] vborbmaxoccupation
        np.ndarray[INT_t] electronindex
        np.ndarray[INT_t] holeindex
        np.ndarray levels
        
        str line
        list columns

   
    for line in f:
       
        line = line.strip()
        columns = line.split(",")
        particle = Nanoparticle()
        particle.x = float(columns[0])*c.nmtobohr
        particle.y = float(columns[1])*c.nmtobohr
        particle.z = float(columns[2])*c.nmtobohr
        particle.diameter = float(columns[3])*c.nmtobohr - 2.0*sample.ligandlength  #diam(i)*nmtobohr-2.0_DP*ligandlength
        particle.radius = particle.diameter / 2.0
        particle.set_cbenergy1()
        particle.set_cbenergy2()
        particle.set_vbenergy()

        
        orboccupation = np.zeros(nbnd, dtype = np.int)
        vborboccupation = np.zeros(nbnd, dtype = np.int)
        orbmaxoccupation = np.zeros(nbnd, dtype = np.int)
        vborbmaxoccupation = np.zeros(nbnd, dtype = np.int)
        #electronindex = np.zeros(nbnd, dtype = np.int)
        #holeindex = np.zeros(nbnd, dtype = np.int)
        
        
        for j in range(nbnd):
            orbmaxoccupation[j] = degeneracy
            vborbmaxoccupation[j] = degeneracy
        
        # assign correct electrons and holes slots in each orbital
        #electronindex.resize(orbmaxoccupation.sum())
        electronindex = np.zeros(orbmaxoccupation.sum(), dtype = np.int)
        electronindex.fill(-1)

        
        #holeindex.resize(orbmaxoccupation.sum())
        holeindex = np.zeros(orbmaxoccupation.sum(), dtype = np.int)
        holeindex.fill(-1)
           
        
        particle.orboccupation = orboccupation
        particle.orbmaxoccupation = orbmaxoccupation
        particle.vborboccupation = vborboccupation
        particle.vborbmaxoccupation = vborbmaxoccupation
        particle.electronindex = electronindex
        particle.holeindex = holeindex
                
        nanoparticles[i] = particle

        i = i+1
 
        #config[columns[0]] = read_float(columns[2])
        
    f.close()
    
    levels = np.asarray([util.get_levels(nanoparticles)])
    
    sample.FWHM = util.fit_gaussian(levels, 50, False)   # needs more inspection, about the range of the fitting

    return nanoparticles

    
    cdef: 
        FLOAT_t packingfraction = 0.0
        FLOAT_t npvolume = 0.0
        FLOAT_t dcout = 0.0
        Nanoparticle nanop

    
    # Set dcout fir the sample 
    if (sample.lemmg):
        for nanop in nanops:
            npvolume = npvolume + 4.0 /3.0 * nanop.radius**3*c.pi
        packingfraction = npvolume / (sample.cellx*sample.celly*sample.cellz)
        dcout = sample.liganddc*(sample.npdc*(1+2*packingfraction)-sample.liganddc*(2*packingfraction-2))/(sample.liganddc*(2+packingfraction)+sample.npdc*(1-packingfraction))
      
    elif (sample.lemla):
        for nanop in nanops:
            npvolume = npvolume + 4.0/3.0*nanop.radius**3*c.pi
        packingfraction=npvolume/(sample.cellx*sample.celly*sample.cellz)
        dcout=(1-packingfraction)*sample.liganddc+packingfraction*sample.npdc     
     
    elif (sample.lemlll):
        print('running here')
        for nanop in nanops:
            npvolume = npvolume + 4.0 / 3.0 *nanop.radius**3*c.pi
        packingfraction = npvolume/(sample.cellx*sample.celly*sample.cellz)
        dcout = ((1-packingfraction)*sample.liganddc**(1.0 /3.0 )+packingfraction*sample.npdc**(1.0 /3.0 ))**3
  
    elif (sample.lempnone):
        print('running here xx')
        dcout = sample.liganddc

    sample.dcout = dcout
    sample.packingfraction = packingfraction
    