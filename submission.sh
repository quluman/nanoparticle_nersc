#!/bin/bash
#$ -cwd
#$ -j no
#$ -N e_50_sample_10
#$ -S /bin/bash
#$ -e e_50_sample_10.err
#$ -o e_50_sample_10.out
#$ -l qname=all.q

source /share/apps/environment.sh

./main_nanda.py 50 400 0.3 3
