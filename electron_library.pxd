#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True, initializedcheck = False


cimport constants as c
from particle_class cimport Sample, Nanoparticle, Electrode, Charge, s_Charge
from event_class cimport Hopping, s_Hopping
from utility cimport npnpdistance
import numpy as np
cimport numpy as np
from libc.math cimport sqrt, fmin
from libc.math cimport fabs
from libc.math cimport exp
from libc.math cimport round



ctypedef np.float64_t FLOAT_t
ctypedef np.int_t INT_t



cdef INT_t throw_electron(Nanoparticle[:] nanops, s_Charge[:] electrons, Sample sample)

cdef INT_t find_events_size(Nanoparticle[:] nanops, Charge[:] electrons, Sample sample)

cdef INT_t find_events(Nanoparticle[:] nanops, Charge[:] electrons, Hopping[:] hoppings, Sample sample)


cdef INT_t execute_event(Nanoparticle[:] nanops, s_Charge[:] elec, s_Hopping executedhopping)

cdef INT_t execute_event_buffer(Nanoparticle[:] nanops, s_Charge[:] elec, s_Hopping executedhopping)


### Funtions used in this part
 
cdef FLOAT_t electronholeeh(Nanoparticle[:] nanops, s_Hopping hopping)

cdef FLOAT_t get_rate(Nanoparticle[:] nanops, s_Hopping hopping, Sample sample, FLOAT_t npdistance)

cdef FLOAT_t poisson(Nanoparticle[:] nanops, s_Hopping hopping, Sample sample)
    
cdef INT_t findelecnewstate(Nanoparticle[:] nanops, INT_t p, INT_t o)

cdef INT_t findelecnewstate_buffer(Nanoparticle targetnp, INT_t p, INT_t o)
 
cdef INT_t find_events_cluster(Nanoparticle[:] nanops, s_Charge[:] electrons, s_Hopping[:] hoppings, Sample sample)

cdef INT_t find_events_cluster_explicit(Nanoparticle[:] nanops, s_Charge[:] electrons, s_Hopping[:] hoppings, Sample sample)

cdef INT_t get_rate_unified(Nanoparticle[:] nanops, s_Hopping[:] hoppings, Sample sample, INT_t sp, INT_t so, INT_t tp, INT_t to, INT_t l, FLOAT_t npdistance, Nanoparticle targetnp, Nanoparticle sourcenp)

cdef INT_t find_events_cluster_both(Nanoparticle[:] nanops, s_Charge[:] electrons, s_Hopping[:] hoppings, Sample sample)