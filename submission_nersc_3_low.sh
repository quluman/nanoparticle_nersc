#!/bin/bash -l

#SBATCH -p regular
#SBATCH --qos=low
#SBATCH -N 1
#SBATCH -t 5:30:00
#SBATCH -J 50_e_100_s_nncoulomb

#module load siesta/3.2-pl-5

#srun -n 1 -c 32 ./mycode.exe

# to export shell variables either use -V in SBATCH or...

export PATH=$PATH:/global/homes/q/quluman/anaconda3/bin/

echo $PATH


python3 /global/homes/q/quluman/nanoparticle_0.01_buffer/main_multicore.py 50 400 0.22 6 0.02


#  electron_number = int(float(sys.argv[1]))    
#  np_number = int(float(sys.argv[2]))    
#  thr_start = float(sys.argv[3])    
#  thr_steps = float(sys.argv[4])
#  thr_resolution = float(sys.argv[5])
