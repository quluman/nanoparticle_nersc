#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True, initializedcheck = False
from particle_class cimport Sample, Nanoparticle, Electrode, Charge, s_Charge

from event_class cimport Hopping, s_Hopping

from electron_library cimport throw_electron, find_events_cluster, execute_event, find_events_cluster_explicit, execute_event_buffer, find_events_cluster_both

from hole_library cimport throw_hole, find_hole_events_cluster, execute_hole_event

cimport routines 

from routines cimport linearsearch, numpysearch, numpysearch_fast, percolation, percolation_full

cimport utility as util
cimport constants as c

import numpy as np
cimport numpy as np
import time

from libc.math cimport log, fabs, round

import matplotlib.pyplot as plt




cpdef monte_carlo(INT_t steps, INT_t sample_no, INT_t e_number, INT_t h_number, INT_t nanops_number, FLOAT_t temp, FLOAT_t thr, str features, FLOAT_t voltage_ratio, output):
    
    
    util.mt_seed()
    
    
    cdef:
        INT_t i,j
        INT_t totalsteps = steps
        FLOAT_t eleccurrent = 0
        FLOAT_t holecurrent = 0
        FLOAT_t totalrate = 0
        FLOAT_t time_ry = 0
        FLOAT_t time_ps = 0
        FLOAT_t mobility = 0
        FLOAT_t icurrent = 0
        FLOAT_t[:,:] data_view = np.zeros([totalsteps,5])       

        FLOAT_t ave_current = 0 
        FLOAT_t ave_mobility = 0
        FLOAT_t ave_rate = 0
 
        FLOAT_t t_start,t_end
        INT_t n_events, n_hevents
        
        
        # Debugging defs
        INT_t intra_count = 0
        INT_t cluster_count = 0
        INT_t normal_count = 0
        INT_t k
        FLOAT_t intra_rate = 0
        FLOAT_t normal_rate = 0
        np.ndarray energy_diff, band_diff
    
    
    t_start = float(time.clock())
    
    # Initialize a sample
    cdef Sample sample = Sample()    
    sample.set_environment()
    
    sample.nelec = e_number
    sample.nhole = h_number
    sample.nnanops = nanops_number
    sample.sample_number = sample_no
    sample.temperature = temp * c.kelvintory
    
    
    
    #if voltage_ratio != 0:    
    #    sample.voltage = sample.temperature * voltage_ratio * 25 / c.sqrt2  # keep eV/kT to be 0.1, over 400 NP samples. electron charge is sqrt2 in Ry unit
        #sample.voltage = 10 * c.kelvintory * 0.1 * 40 / c.sqrt2
    
    #else:
    #sample.voltage = sample.temperature * voltage_ratio * 25 / c.sqrt2   # fixed ratio at 10%
    sample.voltage = 30 * c.kelvintory * 0.1 * 25 / c.sqrt2 # Apply fixed voltage, 40% of that at 30K
    #sample.voltage =  40 * c.kelvintory * voltage_ratio * 25 / c.sqrt2  # apply sliding V relative to T=40K
    
    
    cdef: 
        Nanoparticle[:] nanoparticles
        #Nanoparticle nanop
        #Charge[:] electrons
        #Charge[:] holes
        
        s_Charge s_electrons_def[300]
        s_Charge s_holes_def[300]
        
        
        s_Hopping elec_hopping_def[5000]
        s_Hopping hole_hopping_def[5000]
        s_Hopping exe_event
        
        Electrode source = Electrode()
        Electrode drain = Electrode()
        
    
 
 
    # Initialize nanoparticles
    nanoparticles = routines.set_nanoparticles(sample)
    
    #print('FWHM', sample.FWHM)
    
    sample.ediff_thr = sample.FWHM * c.evtory * thr
    
    # Initialize charges
    cdef s_Charge[:] s_electrons = s_electrons_def 
    cdef s_Charge[:] s_holes = s_holes_def
            
    routines.set_s_electrons(s_electrons, sample)
    routines.set_s_holes(s_holes, sample)
    
    #electrons = routines.set_electrons(sample)
    #holes = routines.set_holes(sample)
    
    
  
    # Initialize sample properties
    routines.dielectrics(nanoparticles, sample)
    
    routines.set_selfenergy(nanoparticles, sample)
    
    
    #routines.neighborlist(nanoparticles, sample)
    routines.neighborlist_direction(nanoparticles, sample)
    #routines.neighborlist_disconnected(nanoparticles, sample)
    
    routines.find_electrode(nanoparticles, source, drain, sample)
    
    #print(percolation_full(nanoparticles, sample))
    
    
    
    
    #nanoparticles = routines.set_nanoparticles(sample)
    #routines.neighborlist(nanoparticles, sample)
    
    
    # Get some constants
    cdef: 
        FLOAT_t cellz = sample.cellz
        FLOAT_t cellz_cm = sample.cellz_nm * 1e-7
        FLOAT_t voltage = sample.voltage
        INT_t nelec = sample.nelec
        FLOAT_t bohrtonm = c.bohrtonm
        FLOAT_t ry_ps = c.ry_ps 
        FLOAT_t volt_ry = c.volt_ry

    
    # Initialize events
    cdef s_Hopping[:] elec_hopping = elec_hopping_def
    
    cdef s_Hopping[:] hole_hopping = hole_hopping_def
    
    for i in range(1000):
        elec_hopping[i].hopping_type = 0
        hole_hopping[i].hopping_type = 1
    
    
    #cdef np.ndarray[FLOAT_t] probability = np.zeros((elec_hopping.shape[0]+hole_hopping.shape[0]), dtype = np.float)
        
    # throw charges    
    throw_electron(nanoparticles, s_electrons, sample)
    
    throw_hole(nanoparticles, s_holes, sample)     
    
    """
    if ((sample_no ==0) and (temp == 50)):
        print(cellz, voltage/volt_ry)
        print(sample.marcusprefac2)
        print(sample.jumpfreq)
        print(sample.emass)
        print(sample.hmass) 
        print(sample.reorgenergy)
        print(sample.ligandlength)
        print(sample.sourcewf)
        print(sample.drainwf)
        print(sample.bradii)
        print(sample.capacitance)
        print('temperature',sample.temperature)       # read from script
        print('voltage',sample.voltage)           # read from script
        print(sample.ndist_thr)
        print(sample.ediff_thr)
        print(sample.dcout)
        print(sample.e_degeneracy)
        print(sample.h_degeneracy)
        print(sample.cellx)
        print(sample.celly)
        print(sample.cellz)
        print(sample.cellz_nm)
        print(sample.beta)
        print(sample.nbnd)
        print(sample.packingfraction)
        print(sample.nelec)    # number of electrons
        print(sample.nhole)    # number of holes
        print(sample.nnanops)   # number of nanoparticles
        print(sample.sample_number)  # store the nanoparticle file to open
        print(sample.npdc)
        print(sample.liganddc)
        print(sample.FWHM)
        print(sample.fitting)
        print(sample.lma)
    """   
        
    sample.fitting = 0.1
    #sample.lpoissonnn = False
    
    if sample_no == 0:
        print('prefactor is', sample.fitting)
        print('voltage is', sample.voltage/c.volt_ry, temp, sample.voltage*c.sqrt2/(sample.temperature*25))
        print('if n-n Coulomb', sample.lpoissonnn)
        
        #print(nanoparticles[0].radius)
        #print(nanoparticles[0].nnindex.base)
        #print(nanoparticles[0].nndistance.base)
        #print(nanoparticles[0].ccdistance.base)
        #print(sample.ndist_thr)
        
        """
        for i in range(nanoparticles.shape[0]):
            for j in nanoparticles[i].nnindex:
            print(nanoparticles[i].selfenergy0)
        """
        

    # start of monte-carlo simulation
    for i in range(totalsteps):
        
     
        # Find electron events
        #n_events = find_events_cluster_explicit(nanoparticles, s_electrons, elec_hopping, sample) 
        n_events = find_events_cluster_both(nanoparticles, s_electrons, elec_hopping, sample) 
        
        # Find hole events
        n_hevents = find_hole_events_cluster(nanoparticles, s_holes, hole_hopping, sample)
        
        
       
       
        
        # Select an event to excute
        exe_event = linearsearch(elec_hopping, hole_hopping, n_events, n_hevents)
        
        
        #intra_count += exe_event.intra
        
        """
        if (i % 10000 == 0):
            energy_diff = np.zeros(n_events)                      #
            band_diff = np.zeros(n_events)
            
            for k in range(n_events):                             #
                
                energy_diff[k] = elec_hopping[k].energydiff       # 
                band_diff[k] = elec_hopping[k].poisson    
            
            print(band_diff.mean()*c.rytoev, energy_diff.mean()*c.rytoev) #energy_diff.std()*c.rytoev)
        """
        """
        if (i % 10000 == 0):
            print('number of events', n_events)

        
        
        if (i % 10000 == 0) and (temp==50):
            print('50', sample.fitting, i, intra_count, float(intra_count)/float(i))
        """

        """
        if ((i % 20000==0)):
            
            normal_count = 0
            cluster_count = 0
            intra_rate = 0
            normal_rate = 0
            
            for k in range(n_events):
                #print(elec_hopping[k].intra, elec_hopping[k].rate)
                if elec_hopping[k].intra == 1:
                    intra_rate += elec_hopping[k].rate
                    cluster_count += 1
                if elec_hopping[k].intra == 0:
                    normal_rate += elec_hopping[k].rate
                    normal_count += 1
                    
            print('prefactor', sample.fitting)
            #print('FWHM', sample.FWHM)
            print('normal hopping', normal_count, normal_rate/normal_count)
            print('intra hopping', cluster_count, intra_rate/cluster_count)
            #print(n_events)
            #print(percolation(nanoparticles, sample))
        
        
        """
        #if exe_event.intra == 0 and temp ==50:
        #    print('normal hopping')
        
        #exe_event = numpysearch(elec_hopping, hole_hopping, n_events, n_hevents, probability)
        #exe_event = numpysearch_fast(elec_hopping, hole_hopping, n_events, n_hevents, rate_array)
        
        
        
        totalrate = exe_event.totalrate     
        
        
        
        if (exe_event.hopping_type == 1):
          
            holecurrent = holecurrent + execute_hole_event(nanoparticles, s_holes, exe_event)
            

        else:    #(exe_event.hopping_type == 0):
    
            eleccurrent = eleccurrent + execute_event_buffer(nanoparticles, s_electrons, exe_event)

        #time_ry = time_ry - log(1-np.random.random()) / totalrate 
        time_ry = time_ry - log(1-util.mt_ldrand()) / totalrate 
 
        time_ps = time_ry * ry_ps
  
        # Get mobility in cm^2 V /s
        mobility = (eleccurrent*cellz*bohrtonm*cellz*bohrtonm)*0.01*volt_ry/(time_ps*voltage*nelec)
        
        # Get current in A
        icurrent = eleccurrent*(1.6e-19) / (time_ps*(1.0e-12))
       
                
        data_view[i,0] = time_ps          # time_ry elec_hoppings.shape[0]
        data_view[i,1] = totalrate / (n_events + n_hevents)
        data_view[i,2] = mobility         #  log(fabs(mobility))     
        #data_view[i,3] = eleccurrent
        data_view[i,4] = icurrent
        # end of simulation loop
    
    
    # Calculate ave_current, in order to get I-V curve
    if features == 'iv':
        for j in range(5000):
            ave_current += data_view[i-j,4]
        ### so the unit of output is really A/cm2, the current density
        output.put([sample_no, voltage_ratio, ave_current*cellz_cm*cellz_cm/5000])   
        
    # I-V for fixed voltage
    if features == 'iv_fixed_V':
        for j in range(5000):
            ave_current += data_view[i-j,4]
        ### so the unit of output is really A/cm2, the current density
        output.put([sample_no, temp, ave_current*cellz_cm*cellz_cm/5000])   
        
        
    # Calculate ave_mobility, in order to get mobility-T curve
    if features == 'mobility':
        for j in range(5000):
            ave_mobility += data_view[i-j,2]
        output.put([sample_no, temp, ave_mobility/5000])
        
        
    if features == 'hotness_map':
        util.visual_hotness(nanoparticles)
        
    if features == 'steady_check':
        plt.plot(data_view[:,0], data_view[:,2])
        plt.yscale('log')
        plt.xlabel('Time [ps]',fontsize = 15)
        plt.ylabel('Mobility [cm^2 V /s]',fontsize = 15)        
        plt.savefig('mobility_time.png')
        plt.show()
        
    if features == 'rate_check':
        for j in range(5000):
            ave_rate += data_view[i-j,1]
        output.put([sample_no, temp, ave_rate/5000])
    
    
    t_end = float(time.clock())        
    
    print("sample %2d at temperature %3d finishes with time %6.2f s" % (sample_no, temp, (t_end-t_start) ))
    
    return 0












cpdef read_packing(INT_t sample_no, INT_t nanoparticle_number, list output): 
    
    util.mt_seed()
      
    cdef:
        INT_t i,j  


    # Initialize a sample
    cdef Sample sample = Sample()    
    sample.set_environment()

    sample.nnanops = nanoparticle_number
    sample.sample_number = sample_no

    
    cdef: 
        Nanoparticle[:] nanoparticles
           
        Electrode source = Electrode()
        Electrode drain = Electrode()
        
    # Initialize nanoparticles
    nanoparticles = routines.set_nanoparticles(sample)
    
    #print('FWHM', sample.FWHM)
  
    # Initialize sample properties
    routines.dielectrics(nanoparticles, sample)
    
    routines.neighborlist(nanoparticles, sample)
    
    routines.find_electrode(nanoparticles, source, drain, sample)
    
    # Get some constants
    cdef: 
        FLOAT_t cellz = sample.cellz
        FLOAT_t cellz_cm = sample.cellz_nm * 1e-7
        FLOAT_t voltage = sample.voltage
        INT_t nelec = sample.nelec
        FLOAT_t bohrtonm = c.bohrtonm
        FLOAT_t ry_ps = c.ry_ps 
        FLOAT_t volt_ry = c.volt_ry
  
    """
    if ((sample_no ==0) and (temp == 50)):
        print(cellz, voltage/volt_ry)
        print(sample.marcusprefac2)
        print(sample.jumpfreq)
        print(sample.emass)
        print(sample.hmass) 
        print(sample.reorgenergy)
        print(sample.ligandlength)
        print(sample.sourcewf)
        print(sample.drainwf)
        print(sample.bradii)
        print(sample.capacitance)
        print('temperature',sample.temperature)       # read from script
        print('voltage',sample.voltage)           # read from script
        print(sample.ndist_thr)
        print(sample.ediff_thr)
        print(sample.dcout)
        print(sample.e_degeneracy)
        print(sample.h_degeneracy)
        print(sample.cellx)
        print(sample.celly)
        print(sample.cellz)
        print(sample.cellz_nm)
        print(sample.beta)
        print(sample.nbnd)
        print(sample.packingfraction)
        print(sample.nelec)    # number of electrons
        print(sample.nhole)    # number of holes
        print(sample.nnanops)   # number of nanoparticles
        print(sample.sample_number)  # store the nanoparticle file to open
        print(sample.npdc)
        print(sample.liganddc)
        print(sample.FWHM)
        print(sample.fitting)
        print(sample.lma)
    """   
    
    if (sample_no % 2 == 0):
        print(sample.packingfraction)
        output.append(sample.packingfraction)
    
    return 0







cpdef find_percolation(INT_t sample_no, INT_t nanoparticle_number, FLOAT_t thr, list output, bint show): 
      
    cdef:
        INT_t i,j  
        FLOAT_t cn_portion


    # Initialize a sample
    cdef Sample sample = Sample()    
    sample.set_environment()

    sample.nnanops = nanoparticle_number
    sample.sample_number = sample_no

    
    cdef: 
        Nanoparticle[:] nanoparticles
           
        Electrode source = Electrode()
        Electrode drain = Electrode()
        
    # Initialize nanoparticles
    nanoparticles = routines.set_nanoparticles(sample)
    
    #print('FWHM', sample.FWHM)
    sample.ediff_thr = sample.FWHM * c.evtory * thr
  
    # Initialize sample properties
    routines.dielectrics(nanoparticles, sample)
    
    
    # Establish neighborlist with directional selection and no pbc in z
    cn_portion = routines.neighborlist_percolation(nanoparticles, sample)

    #print(cn_portion)
    
    # Find electrodes for both sets
    routines.find_electrode(nanoparticles, source, drain, sample)
    
    
    # Determine if there is percolation
    #print(percolation_full(nanoparticles, sample, show))
    
    output.append([cn_portion,percolation_full(nanoparticles, sample, show)])
    
  

    #if (sample_no % 2 == 0):
    #    print(sample.packingfraction)
    #    output.append(sample.packingfraction)
    
    return 0
