# README #

This is the Cython version of the Nanoparticle charge transport project.

### What is this repository for? ###

* Simulates electron/hole transport in nanoparticles

### How do I get set up? ###

* Download
* make
* Use main.py to run

### Authors ###
* Luman Qu, Marton Voros
* lmqu@ucdavis.edu